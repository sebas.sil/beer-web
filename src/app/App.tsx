import React, {useEffect, useState} from 'react'
import {Beer, BeerType, BeerProps} from './component/beer/Beer'
import {Server} from './server'
import {Pagination} from './component/pagination/Pagination'
import {usePagination} from "./context/PaginationContext";

import styles from './App.module.css'
import {Detail} from "./component/detail/Detail";
import {PopupProvider, usePopup} from "./context/PopupContext";

function App() {

  const {getAll} = Server()
  const [beers, setBeers] = useState<BeerType[]>([])
  const {setSelected} = usePopup()
  const {page} = usePagination()

  useEffect(() => {
    getAll(page).then(setBeers)
  }, [page])

  return (
    <>
      <header>

        <img/>
        <nav>

        </nav>

      </header>
        <main>
          <section className={styles.section}>
            <ul className={styles.list}>
              {beers.map(e => <Beer key={e.id} beer={e} setSelected={() => setSelected(e)}/>)}
            </ul>
          </section>
          <section>
            <Pagination/>
          </section>
          <Detail/>
        </main>
      <footer>

      </footer>
    </>
  );
}

export default App;
